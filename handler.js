module.exports.hello = async (event) => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Go Serverless v2.0! Your function executed successfully!',
        input: event,
      },
      null,
      2
    ),
  };
};

export default async (event) => {
	const service = process.env?.SERVICE;
	const stage = process.env?.STAGE;
	const username = process.env?.USERNAME;
	const table = process.env?.EMPLOYEES_TABLE_NAME;
	//const table = process.env?.PETS_TABLE_NAME;
	return {
		statusCode: 200,
	body: JSON.stringify(
		{
		message: `${service}-${stage}-${username}-${table}`,
		input: process.env
		},
		null,
		2
		),
	};
  };